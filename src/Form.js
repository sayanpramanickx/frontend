import { useState } from "react"
import Axios from 'axios'


export default function Form(){
    const [name,setName] = useState('')
    const [age, setAge] = useState('')
    const [sex, setSex] = useState('')
    const [state, setState] = useState('')

    const submitHandler = (e) => {
        e.preventDefault()
        const data = {name, age, sex, state}
        setName('')
        setAge('')
        setSex('')
        setState('')
        // console.log(data);
        Axios.post('http://localhost:5000/submit',data).then(res=>console.log(res)).catch(err=>console.log(err))

    }
    return(
        <div>
            <form onSubmit={submitHandler}>
                <h1>Personal Details Form</h1><br/>
                <input onChange={e=>setName(e.target.value)} value={name} placeholder='Enter Name'/><br/><br/>
                <input onChange={e=>setAge(e.target.value)} value={age} placeholder='Enter Age'/><br/><br/>
                <input onChange={e=>setSex(e.target.value)} value={sex} placeholder='Enter Sex'/><br/><br/>
                <input onChange={e=>setState(e.target.value)} value={state} placeholder='Enter State'/><br/><br/>
                <button>Submit</button>
            </form>
        </div>
    )
}